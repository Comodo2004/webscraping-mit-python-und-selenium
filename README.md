# Webscraping mit Python und Selenium

Dieses Repository ist für unser LB3 Projekt im Modul 214.

Hier ist der Code, welchen wir im Tutorial geschrieben haben, sowie das Tutorial selbst verfügbar.

Um den Code in diesem Repository verwenden zu können, muss man neben Python auch folgende Python-Module installiert haben: 
1. selenium
2. webdriver-manager

Um diese Module zu installieren muss man nach der Installation von Python (z.B über den Microsoft Store) in der Kommandozeile folgende Befehle ausführen: 
```
pip install selenium
```
```
pip install webdriver-manager
```
