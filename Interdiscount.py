# Import der benötigten Python Bibliotheken
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import time

# Installiert den ChromeDriver und speichert ihn in die Variable driver
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))

# ChromeDriver holt sich die Interdiscount Webseite
driver.get("https://interdiscount.ch/de") 

# Sucht die Eingabe auf der Webseite und füllt die Benutzereingabe ein
search_field = driver.find_element(By.CLASS_NAME, "_3fsIMp")
time.sleep(0.5)
search_field.send_keys('Nvidia RTX') # <- String mit Produktenamen ersetzen
search_field.send_keys(Keys.RETURN)
time.sleep(0.5)

# Speichert den Text dieses Elementes in die Variable gefunden
gefunden = driver.find_element(By.XPATH, "//h1[@class='uIyEJC _1EbEh9 _9YoDdk']").text

# Überprüft ob das wort 'gefunden' in der Variable gefunden vorkommt
if 'gefunden' in gefunden:
  # Falls ja, wird Name und Preis geholt, gespeichert und ausgegeben
  product_name = driver.find_element(By.XPATH, "//h4[@class='uIyEJC _2sh9pz _2mLeUk _9YoDdk']").text
  name = product_name.replace("\n", " ")
  print(name)
  product_franken = driver.find_element(By.XPATH, "//div[@class='_3H04_H']").text
  franken = product_franken.replace("\n", " ")
  print(franken)
else:
  # Falls nein, wird nur eine Fehlermeldung ausgegeben.
  print('Es wurde kein Produkt gefunden.')

# Beendet den ChromeDriver
driver.quit()
